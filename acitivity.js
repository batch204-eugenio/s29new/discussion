db.users.insertMany([
    {
        "firstName": "Stephen",
        "lastName": "Hawking",
        "age": 76,
        "email": "stephenhawking@mail.com",
        "department": "HR"
    },

    {
        "firstName": "Neil",
        "lastName": "Armstrong",
        "age": 82,
        "email": "neilarmstrong@mail.com",
        "department": "HR"
    },

    {
        "firstName": "Bill",
        "lastName": "Gates",
        "age": 65,
        "email": "billgates@mail.com",
        "department": "Operations"
    },

    {
        "firstName": "Jane",
        "lastName": "Doe",
        "age": 21,
        "email": "janedoe@mail.com",
        "department": "HR"
    }
]);


/*
Find users with letter s in their first name or d in their last name.
a. Use the $or operator.
b. Show only the firstName and lastName fields and hide the _id field.
*/


db.users.find({
    $or: [
        {"firstName": {$regex: "s", $options: "$i"}},
        {"lastName": {$regex: "d", $options: "$i"}}
    ]
},
{
        "_id": 0,        //we can't use 0 and 1 at the same time, ID is the only EXCEPCTION in FIELD PROJECTION.
        "firstName": 1,
        "lastName": 1
}
);




/*
Find users who are from the HR department and their age is greater
than or equal to 70.
a. Use the $and operator
*/
db.users.find({
    $and: [
        {"department": {
                $in: ["HR"]
         }},
         {"age": {$gt: 70}} 
    ]
        
});



/*
Find users with the letter e in their first name and has an age of less
than or equal to 30.
a. Use the $and, $regex and $lte operators
*/
db.users.find({
    $and: [
        {"firstName": $regex: "e", $options: $i},
        {"age": {$lte: 30}}
    ]
});


