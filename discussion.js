                        ///GIVEN///
                db.inventory.insertMany([
                    {
                        "name": "JavaScript for Beginners",
                        "author": "James Doe",
                        "price": 5000,
                        "stocks": 50,
                        "publisher": "JS Publishing House"
                    },
                    {
                        "name": "HTML and CSS",
                        "author": "John Thomas",
                        "price": 2500,
                        "stocks": 38,
                        "publisher": "NY Publishers"
                    },
                    {
                        "name": "Web Development Fundamentals",
                        "author": "Noah Jimenez",
                        "price": 3000,
                        "stocks": 10,
                        "publisher": "Big Idea Publishing"
                    },
                    {
                        "name": "Java Programming",
                        "author": "David Michael",
                        "price": 10000,
                        "stocks": 100,
                        "publisher": "JS Publishing House"
                    }
                ]);

                        ///COMPARISON QUERY OPERATORS///
///NOTE: This is applicable not only to FIND but also UPDATE and DELETE
///////////////////////////////////////////////////////
//1. $gt operator       gt means greater than
//2. Sgte operator      gte means gretaer than or equal
/*
    Syntax:
        db.collectionName.find({"field": { $gt: value}} );
        db.collectionName.find({"field": { $gte: value}} );
*/
///$GREATER THAN ========================================
        db.inventory.find({
            "stocks": {
                $gt: 50
            }
        });

///$GREATER THAN EQUAL ========================================
        db.inventory.find({
            "stocks": {
                $gte: 50
            }
        });
///////////////////////////////////////////////////////
//1. $lt operator
//2. $lte operator
/*
    Syntax:
        db.collectionName.find({"field": { $lt: value}} );
        db.collectionName.find({"field": { $lte: value}} );
*/

///$LESS THAN ========================================
        db.inventory.find({
            "stocks": {
                $lt: 50
            }
        });

///$LESS THAN EQUAL ========================================
        db.inventory.find({
            "stocks": {
                $lte: 50
            }
        });
///////////////////////////////////////////////////////
//1. $ne operator
/*
    Allows us to find documents that have field values that is NOT EQUAL to
    a specifiec value.

    Syntax:
        db.collectionName.find({"field": { $ne: value}} );
*/

///$NOT EQUAL  ========================================
        db.inventory.find({
            "stocks": {
                $ne: 50
            }
        });
///////////////////////////////////////////////////////
//1.$eq operator
/*
    
    Syntax:
        db.collectionName.find({"field": { $eq: value}} );
*/
//$EQUAL ========================================
        db.inventory.find({
            "stocks": {
                $eq: 50
            }
        });
///////////////////////////////////////////////////////
//1. $in operator using [array or values] in one/same field
/*
    Allows us to find documents with specific MATCH CRITERIA in same or ONE field
    using different VALUES
    Syntax:
        db.collectionName.find({"field": { $in: [array or values]}} );
*/
        db.inventory.find({
            "price": {
                $in: [ 10000, 5000 ]
            }
        });
///////////////////////////////////////////////////////
//1. $nin operator using [array or values] in one/same field
/*
    Allows us to find documents that will NOT MATCH CRITERIA we are looking for
    Syntax:
        db.collectionName.find({"field": { $nin: [array or values]}} );
*/
db.inventory.find({
            "price": {
                $nin: [ 10000, 5000 ]
            }
        });

                        ///LOGICAL QUERY OPERATORS///
///////////////////////////////////////////////////////
//1. $or operator using [ ]
/*
    Checking PROPERTIES and VALUES
    Syntax:
        db.collectionName.find({
            $or: [
                {fieldA: valueA},
                {fieldA: valueA},
            ]
        })
*/
db.inventory.find({
    $or: [
        {"publisher": "JS Publishing House"},
        {"name": "HTML and CSS"}
    ]
});

//using mulitiple operators
//e.g. $or and $lte
db.inventory.find({
    $or: [
        {"author": "James Doe"},
        {"price": {
                $lte: 3000
            }
        }
    ]
});

//1. $and operator
/*
    Syntax:
        db.collectionName.find({ $and: [ {fieldA: valueA}, {fieldB: valueB},]})
*/
db.inventory.find({
    $and: [
        {"stocks": {
            $ne: 50
             }
        },
        {"price": {
            $ne: 5000
            }
        }
    ]
});

//by default parehas lang ito sa nasa taas na $and operator.
db.inventory.find({
    "stocks": {
        $ne:50
    },
    "price": {
        $ne: 5000
    }
});

                ///FIELD PROJECTION///

///////////////////////////////////////////////////////
//1. Inclusion
//find and include the ones with 1 or truth
//number 1 means show
/*
    Syntax:
        db.collectionName.find({criteria}, {field: 1})
*/
//In this example the ID is still included.
db.inventory.find({
    "publisher": "JS Publishing House"
     },
     {
        "name": 1,
        "author": 1,
        "price": 1
     }
);

//2. Exclusion
//number 0 means don't show
//find and exclude the ones with 0 or false
db.inventory.find({
        "author": "John Thomas"
     },
     {
        "price": 0,
        "stocks": 0
     }
);

//3. Suppression or hiding something
//e.g. ID -  //we can't use 0 and 1 at the same time,
//ID is the only EXCEPCTION in FIELD PROJECTION, FIELD INCLUSION and FIELD EXCLUSION
db.inventory.find({
        "price": {
            $lte: 5000
        }
     },
     {
        "_id": 0,        //we can't use 0 and 1 at the same time, ID is the only EXCEPCTION in FIELD PROJECTION.
        "name": 1,
        "author": 1
     }
);

///EVALUATION QUERY OPERATORS///
//1. $regex operator
/*
    Syntax:
        db.collectionName.find({field: $regex: "pattern", $options: "$optionValue"})
*/
//Case Sensitive Query doesn't need option
db.inventory.find({
    "author": {
        $regex: "M"         //means author with letter 'M'
    }
});

//Case Non Sensetive Query needs option
db.inventory.find({
    "author": {
        $regex: "M" ,       //means author with letter 'M'
        $options: "$i"      //$i means, lowercase and uppercase will be accepted  
    }
});


